#!/bin/bash

## 04_02_using_docker_cmd_as_root on ubuntu WHEN WE WORK AS ROOT


############### Using the DOCKER Command ######################

# Checking the confirmation that the user is now added to the docker group by typing:

    id -nG

# Sample output:   kris adm cdrom sudo dip plugdev lpadmin sambashare docker


## (Optional) If you need to add a user to the docker group that you're not logged in as, declare that username explicitly using:
##   sudo usermod -aG docker username

## The syntax is "a chain of options and commands followed by arguments". Usage:
##    docker [OPTIONS] COMMAND [arguments]

# Viewing the version:

    docker --version

# Viewing all available subcommands:

    docker

# Runing 'docker COMMAND --help' for more information on a specific command:

    docker kill --help

# Viewing system-wide information about Docker. We can use:

    docker info

############### Working with DOCKER Images ######################

# Checking if we can access and download images from Docker Hub. We can type:

    docker run hello-world

# We can search for images available on Docker Hub by typing:

    docker search ubuntu

# Executing the following command to download the official "ubuntu" image to our machine:

    docker pull ubuntu

# Viewing the images that have been downloaded to our machine. We can type:

    docker images

############### Running a DOCKER Container ######################

# The "hello-world" container is an example container, which runs and exits after emitting a test message.
# Containers can be much more useful than that, and they can be interactive, as a virtual machines.

# As an second example, let's run a container using the latest official image of "Ubuntu".
# The combination of the -i and -t switches gives you interactive shell access into the container. We can use "bash":

    docker run -it ubuntu bash

# Your command prompt should change to reflect the fact that you're now working inside the container:

## OUTPUT:

#kris@gandalf1:~$     docker run -it ubuntu bash
#root@5495e623ad66:/#

# Pay attention! Note the container ID in the command prompt: it is 5495e623ad66.
# You'll need that container ID later to identify the container when you want to remove it.


# Now you can run any command inside the container:

    apt update

# Installing any application in it. Let's install the Node.js:

    apt install -y nodejs

# It installs Node.js in the container from the official Ubuntu repository.
# "Do you want to continue? [Y/n]"
### >> type "y" + ENTER
# Verify that Node.js is installed to see the version number:

    node -v

# Any changes you make inside the container only apply to that container.
# To exit the container, type "exit" at the prompt.

    exit

############### Managing DOCKER Containers ######################

# Viewing all containers: active and inactive. Type with the "-a" switch:

    docker ps -a

# Viewing the latest container you created:

    docker ps -l

