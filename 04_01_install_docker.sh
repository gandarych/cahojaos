#!/bin/bash

## 04_01_install_docker on ubuntu

############### Installing DOCKER ######################

# Try to install the DOCKER:

sudo apt install -y docker.io
### >> Pay attention for every "Do you want to continue? [Y/n]".
### Accept "y" if it occurs. Try to enable silent mode of installation again...

sudo docker

sudo docker --version

# If something is missing or not, next let's try to use advices of Brian Hogan at his article:
# on https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

# Updating the list of packages:

    sudo apt update
### >> enter the password

# Installing some prerequisite packages which let apt use packages over HTTPS:

    sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# Adding the GPG key for the official Docker repository to your system:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Adding the Docker repository to APT sources:

    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

# Updating the list of packages database with Docker from the newly added repo:

    sudo apt update

# Making sure we are about to install from the Docker repo (instead of the default Ubuntu repo):

    sudo apt-cache policy docker-ce

# Installing Docker - Container Engine:

    sudo apt install -y docker-ce

# Checking: Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it's running:

    sudo systemctl status docker

## OUTPUT:
####  *  docker.service - Docker Application Container Engine
####    Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
####    Active: active (running) since Sun RRRR-MM-DD 15:05:33 CET; 5s ago
####      Docs: https://docs.docker.com
####   Main PID: 20373 (dockerd)
####     Tasks: 12
####    CGroup: /system.slice/docker.service
####             └─20373 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock


############### Executing the DOCKER Command Without Sudo ######################

    sudo docker run --help

# If you want to avoid typing "sudo" whenever you run the docker command, add your username to the docker group:

    echo ${USER}
    sudo usermod -aG docker ${USER}

# Apllying the new group membership. Type the following:

    su - ${USER}
### >> enter the password

# Checking the confirmation that the user is now added to the docker group by typing:

    id -nG

# Sample output:   kris adm cdrom sudo dip plugdev lpadmin sambashare docker


## (Optional) If you need to add a user to the docker group that you're not logged in as, declare that username explicitly using:
##   sudo usermod -aG docker username